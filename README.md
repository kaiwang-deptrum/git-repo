# repo

Repo is a tool built on top of Git.  Repo helps manage many Git repositories,
does the uploads to revision control systems, and automates parts of the
development workflow.  Repo is not meant to replace Git, only to make it
easier to work with Git.  The repo command is an executable Python script
that you can put anywhere in your path.

* Homepage: https://gerrit.googlesource.com/git-repo/
* Bug reports: https://code.google.com/p/git-repo/issues/
* Source: https://gerrit.googlesource.com/git-repo/
* Overview: https://source.android.com/source/developing.html
* Docs: https://source.android.com/source/using-repo.html
* [repo Manifest Format](./docs/manifest-format.md)
* [repo Hooks](./docs/repo-hooks.md)
* [Submitting patches](./SUBMITTING_PATCHES.md)
* 

# 操作方法

## 配置运行环境

1. 安装python（windows上测试成功的版本为python2.7，更高版本未曾尝试）

2. 获取repo
   -  git clone git@gitlab.com:kaiwang-deptrum/git-repo.git
3. 将git-repo路径 添加致系统环境变量path

4. 创建仓库根目录（非必须）

5. 进入仓库根目录，执行如下命令

    5.1 repo init -u git@gitlab.com:kaiwang-deptrum/manifest.git(在windows系统初次使用时，需要以管理员的身份运行cmd)
    5.2 repo sync

# 更多repo命令可参阅 [此链接](https://source.android.google.cn/source/developing.html)







